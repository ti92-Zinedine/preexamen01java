package com.example.preexamen01java;

public class ReciboNomina {
    private int numRecibo;
    private String Nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    // Constructor ReciboNomina()
    public ReciboNomina(int numRecibo, String Nombre, float horasTrabNormal, float horasTrabExtras,
                        int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.Nombre = Nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    // Métodos Set y Get
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    // Métodos para cálculo de nómina
    public float calcularSubtotal(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float pagoBase = 200;
        float subTotal;

        if(puesto == 1){
            pagoBase = (float) (pagoBase + (pagoBase * 0.20));
        }else if(puesto == 2) {
            pagoBase = (float) (pagoBase + (pagoBase * 0.50));
        }else if(puesto == 3){
            pagoBase = (float) (pagoBase + (pagoBase * 1.00));
        }else {
            System.out.println("No es posible calcular");
        }
        subTotal = (pagoBase * horasTrabNormal) + (pagoBase * horasTrabExtras * 2);
        return subTotal;
    }

    public float calcularImpuesto(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float impuesto = (float) (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16);
        return impuesto;
    }

    public float calcularTotal(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float subTotal = calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras);
        float impuesto = calcularImpuesto(puesto, horasTrabNormal, horasTrabExtras);
        float total = subTotal - impuesto;
        return total;
    }
}
