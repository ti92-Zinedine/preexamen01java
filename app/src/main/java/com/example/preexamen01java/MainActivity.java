package com.example.preexamen01java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombreTrabajador;
    private TextView lblNombreTrabajador;

    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador);
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void entrar() {
        String strNombre;

        strNombre = getApplicationContext().getResources().getString(R.string.nombre);

        if(strNombre.equals(txtNombreTrabajador.getText().toString().trim())) {
            // Hacer el paquete para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreTrabajador.getText().toString());

            // Crear el intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent);
        } else {
            Toast.makeText(this.getApplicationContext(), "El usuario no es válido", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        finish();
    }
}